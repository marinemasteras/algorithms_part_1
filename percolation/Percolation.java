import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private final WeightedQuickUnionUF wqu;
    private final WeightedQuickUnionUF wquNoBottom;
    private boolean[][] grid;
    private int counter;
    private final int size;
    private final int top;
    private final int bottom;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if (1 > n) {
            throw new IllegalArgumentException();
        }
        this.size = n;
        this.top = 0;
        this.bottom = n * n + 1;
        this.wqu = new WeightedQuickUnionUF(n * n + 2);
        this.wquNoBottom = new WeightedQuickUnionUF(n * n + 1);
        this.grid = new boolean[n][n];
        this.counter = 0;
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {

        isInputValid(row, col);

        if (isOpen(row, col)) {
            return;
        }
        counter++;
        grid[row - 1][col - 1] = true;

        if (row == 1) {
            wqu.union(getWeightedQuickFindIndex(row, col), top);
            wquNoBottom.union(getWeightedQuickFindIndex(row, col), top);

        }
        if (row == size) {
            wqu.union(getWeightedQuickFindIndex(row, col), bottom);
        }
        if (row > 1 && isOpen(row - 1, col)) {
            wqu.union(getWeightedQuickFindIndex(row, col), getWeightedQuickFindIndex(row - 1, col));
            wquNoBottom.union(getWeightedQuickFindIndex(row, col),
                              getWeightedQuickFindIndex(row - 1, col));
        }
        if (row < size && isOpen(row + 1, col)) {
            wqu.union(getWeightedQuickFindIndex(row, col), getWeightedQuickFindIndex(row + 1, col));
            wquNoBottom.union(getWeightedQuickFindIndex(row, col),
                              getWeightedQuickFindIndex(row + 1, col));
        }
        if (col > 1 && isOpen(row, col - 1)) {
            wqu.union(getWeightedQuickFindIndex(row, col), getWeightedQuickFindIndex(row, col - 1));
            wquNoBottom.union(getWeightedQuickFindIndex(row, col),
                              getWeightedQuickFindIndex(row, col - 1));
        }
        if (col < size && isOpen(row, col + 1)) {
            wqu.union(getWeightedQuickFindIndex(row, col), getWeightedQuickFindIndex(row, col + 1));
            wquNoBottom.union(getWeightedQuickFindIndex(row, col),
                              getWeightedQuickFindIndex(row, col + 1));
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        isInputValid(row, col);
        return grid[row - 1][col - 1];
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        isInputValid(row, col);
        return wquNoBottom.find(getWeightedQuickFindIndex(row, col)) == wquNoBottom.find(top);
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return counter;
    }

    // does the system percolate?
    public boolean percolates() {
        return wqu.find(top) == wqu.find(bottom);
    }

    private int getWeightedQuickFindIndex(int row, int col) {
        return (row - 1) * size + col;
    }

    private void isInputValid(int row, int col) {
        if ((1 <= row && row <= size) && (1 <= col && col <= size)) {
            return;
        }
        throw new IllegalArgumentException();
    }
}