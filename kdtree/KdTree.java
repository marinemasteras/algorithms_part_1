/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.awt.Color;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class KdTree {
    // private TreeSet<Point2D> point2DSet;

    private Node root;             // root of KdTree
    private int size;
    private Point2D nearestPoint;

    private static class Node {
        private Point2D p;           // value
        private final boolean isVertical;
        private Node ld;  // left/bottom subtree
        private Node rt;        // right/top subtree

        public Node(Point2D p, Node ld, Node rt, boolean isVertical) {
            this.p = p;
            this.isVertical = isVertical;
            this.ld = ld;
            this.rt = rt;
        }
    }


    /* see previous slide */
    // construct an empty set of points
    public KdTree() {
        this.size = 0;
        this.root = null;
        this.nearestPoint = null;
    }

    // is the set empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // number of points in the set
    public int size() {
        return this.size;
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        root = insert(root, p, true);
    }

    private Node insert(Node parentNode, Point2D p, boolean isVertical) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        if (parentNode == null) {
            size++;
            return new Node(p, null, null, isVertical);
        }

        if (parentNode.p.y() == p.y() && parentNode.p.x() == p.x()) {
            return parentNode;
        }

        if ((parentNode.isVertical && parentNode.p.x() > p.x()) || (!parentNode.isVertical
                && parentNode.p.y() > p.y())) {
            parentNode.ld = insert(parentNode.ld, p, !parentNode.isVertical);
            Set<String> x = new HashSet<>();
        }
        else {
            parentNode.rt = insert(parentNode.rt, p, !parentNode.isVertical);
        }
        return parentNode;
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        return get(p) != null;
    }

    private Point2D get(Point2D p) {
        return get(root, p);
    }

    private Point2D get(Node parentNode, Point2D p) {
        if (parentNode == null) {
            return null;
        }
        if (parentNode.p.x() == p.x() && parentNode.p.y() == p.y()) {
            return parentNode.p;
        }
        if ((parentNode.isVertical && parentNode.p.x() > p.x()) || (!parentNode.isVertical
                && parentNode.p.y() > p.y())) {
            return get(parentNode.ld, p);
        }
        else {
            return get(parentNode.rt, p);
        }

    }

    // draw all points to standard draw
    public void draw() {
        draw(root, new RectHV(0, 0, 1, 1));
    }

    private void draw(Node node, RectHV prevRect) {
        if (node == null) {
            return;
        }
        StdDraw.setPenColor(Color.black);
        StdDraw.point(node.p.x(), node.p.y());
        if (node.isVertical) {
            StdDraw.setPenColor(Color.red);
            StdDraw.line(node.p.x(), prevRect.ymin(), node.p.x(), prevRect.ymax());
            draw(node.rt,
                 new RectHV(node.p.x(), prevRect.ymin(), prevRect.xmax(), prevRect.ymax()));
            draw(node.ld,
                 new RectHV(prevRect.xmin(), prevRect.ymin(), node.p.x(), prevRect.ymax()));
        }
        else {
            StdDraw.setPenColor(Color.blue);
            StdDraw.line(prevRect.xmin(), node.p.y(), prevRect.xmax(), node.p.y());
            draw(node.rt,
                 new RectHV(prevRect.xmin(), node.p.y(), prevRect.xmax(), prevRect.ymax()));
            draw(node.ld,
                 new RectHV(prevRect.xmin(), prevRect.ymin(), prevRect.xmax(), node.p.y()));
        }

    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new IllegalArgumentException();
        }
        List<Point2D> results = new LinkedList<>();
        range(results, root, rect);

        return results;
    }

    private void range(List<Point2D> results, Node node, RectHV rect) {
        if (node == null) {
            return;
        }
        if (doesIntersect(rect, node.p.x(), node.p.y())) {
            results.add(node.p);
        }
        if (node.isVertical) {
            if (rect.xmin() < node.p.x()) {
                range(results, node.ld, rect);
            }
            if (rect.xmax() >= node.p.x()) {
                range(results, node.rt, rect);
            }
        }
        else {
            if (rect.ymin() < node.p.y()) {
                range(results, node.ld, rect);
            }
            if (rect.ymax() >= node.p.y()) {
                range(results, node.rt, rect);
            }
        }
    }

    private boolean doesIntersect(RectHV rect, double x, double y) {
        return (rect.xmax() >= x && rect.xmin() <= x) && (rect.ymax() >= y && rect.ymin() <= y);
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        if (isEmpty()){
            return  null;
        }
        nearestPoint = root.p;
        return nearest(p, root, new RectHV(0, 0, 1, 1));
    }

    private Point2D nearest(Point2D p, Node node, RectHV nodeRect) {
        if (node == null) {
            return nearestPoint;
        }
        if (nearestPoint.distanceSquaredTo(p) > nodeRect.distanceSquaredTo(p)) {
            if (closestRoute(p, node.p) < closestRoute(p, nearestPoint)) {
                nearestPoint = node.p;
            }
            if ((node.isVertical && node.p.x() < p.x()) || (!node.isVertical
                    && node.p.y() < p.y())) {
                nearest(p, node.rt, createRectRt(node, nodeRect));
                nearest(p, node.ld, createRectLd(node, nodeRect));
            }
            else {
                nearest(p, node.ld, createRectLd(node, nodeRect));
                nearest(p, node.rt, createRectRt(node, nodeRect));
            }
        }
        return nearestPoint;
    }

    private RectHV createRectRt(Node node, RectHV prevRect) {
        return node.isVertical ?
               new RectHV(node.p.x(), prevRect.ymin(), prevRect.xmax(), prevRect.ymax()) :
               new RectHV(prevRect.xmin(), node.p.y(), prevRect.xmax(), prevRect.ymax());
    }

    private RectHV createRectLd(Node node, RectHV prevRect) {
        return node.isVertical ?
               new RectHV(prevRect.xmin(), prevRect.ymin(), node.p.x(), prevRect.ymax()) :
               new RectHV(prevRect.xmin(), prevRect.ymin(), prevRect.xmax(), node.p.y());
    }


    private double closestRoute(Point2D p, Point2D that) {
        return p.distanceSquaredTo(that);
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        KdTree pointSET = new KdTree();

        for (int i = 0; i < 10; i++) {
            pointSET.insert(new Point2D(0.01, i * 0.01));
        }

    }
}