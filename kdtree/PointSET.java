/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.util.Set;
import java.util.TreeSet;

public class PointSET {
    private final TreeSet<Point2D> point2DSet;

    // construct an empty set of points
    public PointSET() {
        this.point2DSet = new TreeSet<>();
    }

    // is the set empty?
    public boolean isEmpty() {
        return point2DSet.isEmpty();
    }

    // number of points in the set
    public int size() {
        return point2DSet.size();
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        point2DSet.add(p);
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        return point2DSet.contains(p);
    }

    // draw all points to standard draw
    public void draw() {
        point2DSet.forEach(a -> StdDraw.point(a.x(), a.y()));
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new IllegalArgumentException();
        }
        Set<Point2D> pointsInRange = new TreeSet<>();
        for (Point2D p : point2DSet) {
            if (rect.contains(p)) {
                pointsInRange.add(p);
            }
        }
        return pointsInRange;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        if (point2DSet.isEmpty()) {
            return null;
        }
        Point2D minPoint = null;
        for (Point2D checkedPoint : point2DSet) {
            if (minPoint == null || p.distanceSquaredTo(checkedPoint) < p
                    .distanceSquaredTo(minPoint)) {
                minPoint = checkedPoint;
            }
        }
        return minPoint;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
    }
}