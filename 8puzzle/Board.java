/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board {

    private final int n;
    private final int size;
    private int[][] tiles;

    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    public Board(int[][] tiles) {
        this.n = tiles.length;
        this.size = n * n;
        this.tiles = new int[n][n];
        for (int i = 0; i < n; i++) {
            this.tiles[i] = Arrays.copyOf(tiles[i], n);
        }
    }

    // string representation of this board
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(n + "\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                builder.append(String.format("%2d ", tiles[i][j]));
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    // board dimension n
    public int dimension() {
        return n;
    }

    // number of tiles out of place
    public int hamming() {
        int hamming = 0;
        int value = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] != value && value != size) {
                    hamming++;
                }
                value++;
            }
        }
        return hamming;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        int manhattan = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int tile = tiles[i][j];
                if (tile != 0) {
                    int[] goalPos = goalPosition(tile);
                    manhattan += abs(goalPos[0] - i) + abs(goalPos[1] - j);
                }
            }
        }
        return manhattan;
    }

    private int abs(int a) {
        return a < 0 ? -a : a;
    }

    private int[] goalPosition(int tile) {
        int x = (tile - 1) / n;
        int y = (tile - n * x - 1);
        return new int[] { x, y };
    }


    // is this board the goal board?
    public boolean isGoal() {
        return hamming() == 0;
    }

    // does this board equal y?
    public boolean equals(Object y) {
        // self check
        if (this == y)
            return true;
        // null check
        if (y == null)
            return false;
        // type check and cast
        if (getClass() != y.getClass())
            return false;
        Board board = (Board) y;
        // field comparison
       /* return Objects.equals(n, board.n)
                // && Objects.equals(size, board.size)
                && Arrays.deepEquals(tiles, board.tiles);*/
        if (this.n != board.n) return false;
        if (Arrays.deepEquals(tiles, board.tiles)) return false;
        return true;
           }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        List<Board> neighbours = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] == 0) {
                    if (i > 0) {
                        Board neighbour = new Board(tiles);
                        swap(neighbour, i, j, i - 1, j);
                        neighbours.add(neighbour);
                    }
                    if (i < n - 1) {
                        Board neighbour = new Board(tiles);
                        swap(neighbour, i, j, i + 1, j);
                        neighbours.add(neighbour);
                    }
                    if (j > 0) {
                        Board neighbour = new Board(tiles);
                        swap(neighbour, i, j, i, j - 1);
                        neighbours.add(neighbour);
                    }
                    if (j < n - 1) {
                        Board neighbour = new Board(tiles);
                        swap(neighbour, i, j, i, j + 1);
                        neighbours.add(neighbour);
                    }
                    return neighbours;
                }
            }
        }
        return null;
    }

    private void swap(Board board, int i, int j, int iNew, int jNew) {
        int temp = board.tiles[i][j];
        board.tiles[i][j] = board.tiles[iNew][jNew];
        board.tiles[iNew][jNew] = temp;
    }

    // a board that is obtained by exchanging any pair of tiles
    public Board twin() {
        boolean swapped = false;
        int a = -1;
        int b = -1;
        int[][] copy = copy(this.tiles);

        Board board = new Board(copy);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (board.tiles[i][j] == 0) {
                    continue;
                }
                if (a > -1) {
                    board.swap(board, a, b, i, j);
                    swapped = true;
                    break;
                }
                a = i;
                b = j;
            }
            if (swapped) {
                break;
            }
        }
        return board;
    }

    private int[][] copy(int[][] ints) {
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            a[i] = Arrays.copyOf(ints[i], n);
        }
        return a;
    }

    // unit testing (not graded)
    public static void main(String[] args) {
        int val = 0;
        int k = 3;
        int[][] tiles = new int[k][k];
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < k; j++) {
                tiles[i][j] = val;
                val++;
            }
        }

        Board b = new Board(tiles);
        b.swap(b, 0, 0, 1, 1);
        System.out.println(b.toString());
        for (int[] i : tiles) {
            for (int z : i) {
                System.out.println("value:" + z + " position: " + Arrays
                        .toString(b.goalPosition(z)));
            }
        }
        System.out.println("the hamming value is: " + b.hamming());
        System.out.println("the manhattan value is: " + b.manhattan());

        Iterable<Board> neigbours = b.neighbors();
        for (Board n : neigbours) {
            System.out.println(n.toString());
        }

        Board c = new Board(new int[][] { new int[] { 2, 1 }, new int[] { 3, 0 } });
        System.out.println(c.twin());
        System.out.println(c.twin());
    }

}