/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.ResizingArrayStack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {

    private SearchNode result;


    private class SearchNode implements Comparable<SearchNode> {
        private final Board board;
        private int numberOfMoves;
        private final SearchNode previousSearchNode;
        private final int manhattan;


        public SearchNode(Board board, int numberOfMoves, SearchNode previousSearchNode) {
            this.board = board;
            this.numberOfMoves = numberOfMoves;
            this.previousSearchNode = previousSearchNode;
            this.manhattan = board.manhattan();
        }

        public int compareTo(SearchNode searchNode) {
            if (manhattan + this.numberOfMoves
                    > searchNode.manhattan + searchNode.numberOfMoves) {
                return 1;
            }
            else if (manhattan + this.numberOfMoves
                    < searchNode.manhattan + searchNode.numberOfMoves) {
                return -1;
            }
            return 0;
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null) {
            throw new IllegalArgumentException();
        }
        MinPQ<SearchNode> openSet = new MinPQ<>(/*new OpenSetComparator()*/);
        MinPQ<SearchNode> twinOpenSet = new MinPQ<>(/*new OpenSetComparator()*/);

        openSet.insert(new SearchNode(initial, 0, null));
        twinOpenSet.insert(new SearchNode(initial.twin(), 0, null));
        while (!openSet.isEmpty()) {
            SearchNode x = openSet.delMin();
            SearchNode xTwin = twinOpenSet.delMin();

            if (x.board.isGoal()) {
                this.result = x;
                break;
            }
            if (xTwin.board.isGoal()) {
                this.result = x;
                result.numberOfMoves = -1;
                break;
            }

            for (Board neighbour : x.board.neighbors()) {
                if (x.previousSearchNode != null && x.previousSearchNode.board.equals(neighbour)) {
                    continue;
                }
                openSet.insert(new SearchNode(neighbour, x.numberOfMoves + 1, x));
            }
            for (Board neighbourTwin : xTwin.board.neighbors()) {
                if (xTwin.previousSearchNode != null && xTwin.previousSearchNode.board
                        .equals(neighbourTwin)) {
                    continue;
                }
                twinOpenSet.insert(new SearchNode(neighbourTwin, xTwin.numberOfMoves + 1, x));
            }

        }

    }

    // is the initial board solvable? (see below)
    public boolean isSolvable() {
        return this.result.numberOfMoves > -1;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        return this.result.numberOfMoves;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (!isSolvable()) {
            return null;
        }
        ResizingArrayStack<Board> solution = new ResizingArrayStack<>();
        SearchNode fin = this.result;
        while (fin != null) {
            solution.push(fin.board);
            fin = fin.previousSearchNode;
        }
        return solution;
    }

    // test client (see below)
    public static void main(String[] args) {
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board initial = new Board(tiles);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }

}