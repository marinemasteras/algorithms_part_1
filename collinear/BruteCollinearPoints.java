import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class BruteCollinearPoints {

  private final LineSegment[] segments;


  public BruteCollinearPoints(Point[] points) {
    if (points == null) {
      throw new IllegalArgumentException();
    }
    for (Point p : points) {
      if (p == null) {
        throw new IllegalArgumentException();
      }
    }
    Point[] cloned = points.clone();
    Arrays.sort(cloned);
    for (int i = 0; i < cloned.length - 1; i++) {
      if (cloned[i].compareTo(cloned[i + 1]) == 0) {
        throw new IllegalArgumentException();
      }
    }
    ArrayList<LineSegment> segmentList = new ArrayList<>();

    for (int i = 0; i < cloned.length - 3; i++) {

      for (int j = i + 1; j < cloned.length - 2; j++) {
        double slopeIJ = cloned[i].slopeTo(cloned[j]);
        for (int k = j + 1; k < cloned.length - 1; k++) {
          double slopeIK = cloned[i].slopeTo(cloned[k]);
          if (slopeIJ == slopeIK) {
            for (int m = k + 1; m < cloned.length; m++) {
              double slopeIM = cloned[i].slopeTo(cloned[m]);
              if (slopeIJ == slopeIM) {
                LineSegment segment = new LineSegment(cloned[i], cloned[m]);
                if(!segmentList.contains(segment)){
                segmentList.add(segment);}
              }
            }
          }
        }
      }
    }
    this.segments = segmentList.toArray(new LineSegment[0]);
  }


  public int numberOfSegments() {
    return segments.length;
  }      // the number of line segments

  public LineSegment[] segments() {
  List<String> a = new LinkedList<>();
  
    return segments.clone();
  }

}
