import java.util.ArrayList;
import java.util.Arrays;

public class FastCollinearPoints {

  private final ArrayList<LineSegment> segments;
  // private int count;

  public FastCollinearPoints(Point[] points) {
    if (points == null) {
      throw new IllegalArgumentException();
    }
    for (Point p : points) {
      if (p == null) {
        throw new IllegalArgumentException();
      }
    }
    // this.count = 0;
    this.segments = new ArrayList<>();
    int counter = 0;

    Point[] cloned = points.clone();
    Arrays.sort(cloned);
    for (int i = 0; i < cloned.length - 1; i++) {
      if (cloned[i].compareTo(cloned[i + 1]) == 0) {
        throw new IllegalArgumentException();
      }
    }
    for (Point point : points) {
      Arrays.sort(cloned, point.slopeOrder());
      double comparingDouble = point.slopeTo(cloned[1]);
      for (int i = 1; i < cloned.length; i++) {

        if (cloned[0].slopeTo(cloned[i]) == comparingDouble) {
          counter++;
        } else {
          if (counter >= 3) {
            addLineSegment(cloned, counter, i, cloned[0]);
          }
          comparingDouble = cloned[0].slopeTo(cloned[i]);
          counter = 1;
        }
      }
      if (counter >= 3) {
        addLineSegment(cloned, counter, cloned.length, cloned[0]);
      }
      counter = 0;
    }
  }

  public int numberOfSegments() {
    return segments.size();
  }

  public LineSegment[] segments() {
    LineSegment[] segmentsArray = new LineSegment[segments.size()];
    for (int i = 0; i < segmentsArray.length; i++) {
      segmentsArray[i] = segments.get(i);
    }
    return segmentsArray;
  }

  private void addLineSegment(Point[] points, int counter, int i, Point base) {
    Point[] segmentArray = new Point[counter + 1];
    segmentArray[0] = base;
    for (int a = 1; a < counter + 1; a++) {
      segmentArray[a] = points[i - counter - 1 + a];
    }
    Arrays.sort(segmentArray, Point::compareTo);

    if (base.compareTo(segmentArray[0]) == 0) {
      LineSegment segment = new LineSegment(segmentArray[0], segmentArray[counter]);
      segments.add(segment);
      //count++;
    }
  }

  private void swap(Point[] points, int p, int i) {
    Point temp = points[p];
    points[p] = points[i];
    points[i] = temp;
  }
}