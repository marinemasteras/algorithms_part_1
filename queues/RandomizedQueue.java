/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private int size;
    private Item[] items;


    // construct an empty randomized queue
    public RandomizedQueue() {
        this.size = 0;
        items = (Item[]) new Object[2];
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return size;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        if (size == items.length) {
            resizeAndReindex(2 * items.length);
        }
        items[size++] = item;
    }

    // remove and return a random item
    public Item dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue underflow");
        }

        int randomPick = StdRandom.uniform(size);
        Item item = items[randomPick];
        items[randomPick] = items[--size];
        items[size] = null;

        if (size > 0 && size == items.length / 4) {
            resizeAndReindex(items.length / 2);
        }
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        int randomPick = StdRandom.uniform(size);
        return items[randomPick];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new ArrayIterator();
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ArrayIterator implements Iterator<Item> {
        private int i;
        private int[] order;

        public ArrayIterator() {
            i = size;
            order = StdRandom.permutation(i);
        }

        public boolean hasNext() {
            return i > 0;
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = items[order[i - 1]];
            i--;
            return item;
        }
    }


    private Item[] resizeAndReindex(int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < size; i++) {
            copy[i] = items[i];
        }
        items = copy;
        return copy;
    }


    // unit testing (required)
    public static void main(String[] args) {
        RandomizedQueue<Integer> que = new RandomizedQueue<>();

        System.out.println(que.size());
        que.enqueue(1);
        que.dequeue();
        System.out.println(que.size());

        for (int i = 0; i < 10; i++) {
            que.enqueue(i);
            que.enqueue(2 * i);
            que.dequeue();
        }

        Iterator<Integer> it = que.iterator();
        while (it.hasNext()) {
            System.out.println("Size: " + que.size);
            System.out.println("Iterator: " + it.next());
        }


    }
}