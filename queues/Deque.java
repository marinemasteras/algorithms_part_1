/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private class Node {
        private Item item;
        private Node next;
        private Node previous;
    }

    private Node first;
    private Node last;
    private int size;

    public Deque() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        Node oldFirst = first;
        if (isEmpty()) {
            last = oldFirst;
        }
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        first.previous = null;
        if (oldFirst != null) {
            oldFirst.previous = first;
        }
        if (last == null) {
            last = first;
        }
        size++;
    }

    public void addLast(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        Node oldLast = last;
        last = new Node();
        last.item = item;
        last.next = null;
        last.previous = oldLast;

        if (isEmpty()) {
            first = last;
        }
        else {
            oldLast.next = last;
        }
        size++;
    }

    public Item removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Item item = first.item;
        if (first.next == null) {
            first = null;
            last = null;
        }
        else {
            first = first.next;
            first.previous = null;
        }
        size--;
        return item;
    }

    public Item removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Node oldLast = last;
        if (first.next == null) {
            Item item = first.item;
            first = null;
            last = null;
            size--;
            return item;
        }
        last = oldLast.previous;
        last.next = null;
        size--;

        return oldLast.item;
    }

    @Override
    public Iterator<Item> iterator() {
        return new LinkedListIterator();
    }

    private class LinkedListIterator implements Iterator<Item> {

        private Node current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next;
            return item;
        }
    }


    public static void main(String[] args) {
        Deque<String> deque = new Deque<String>();

        String[] arr = { "0", "1", "2", "3", "4", "5", "6" };
        assert (deque.isEmpty());
        assert (deque.size() == 0);
        deque.addFirst(arr[0]);
        deque.removeFirst();
        deque.addFirst(arr[1]);
        deque.removeLast();
        deque.addFirst(arr[2]);
        deque.removeLast();
        System.out.println("size is: " + deque.size());
        deque.addFirst(arr[3]);
        deque.addLast(arr[4]);
        deque.addLast(arr[5]);
        deque.removeLast();

        deque.removeLast();
        deque.addLast(arr[6]);
        Iterator<String> it = deque.iterator();
        System.out.println(it.hasNext());
        while (it.hasNext()) {
            it.next();
        }

    }
}
